/*
 * Copyright (c) 2014 Hyokun Yun
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#ifndef __SOLAR_DATA_HPP
#define __SOLAR_DATA_HPP

#include "solar.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <set>

#include <boost/tokenizer.hpp>

namespace solar {


  using std::cout; using std::cerr; using std::endl;
  using std::ifstream;
  using std::vector;
  using std::set;
  using std::make_pair;

  using boost::lexical_cast;
  using boost::tokenizer;
  using boost::char_separator;

  class Data {

  public:
    Data() : 
      numthreads_(0),
      numrows_per_part_(0)
    {
      
    }

    ~Data() {
      if (numthreads_ > 0) {
	for (int i=0; i < numthreads_; i++) {
	  sallocator<index_type>().deallocate(train_row_nnzs_[i], numrows_per_part_);
	}
	sallocator<index_type *>().deallocate(train_row_nnzs_, numthreads_);

	sallocator<set<index_type>> si_alloc;
	for (int i=0; i < numthreads_; i++) {
	  for (int j=0; j < numrows_per_part_; j++) {
	    si_alloc.destroy(train_rowwise_[i] + j);
	  }
	  si_alloc.deallocate(train_rowwise_[i], numrows_per_part_);
	}
	sallocator<set<index_type> *>().deallocate(train_rowwise_, numthreads_);

	for (int i=0; i < numthreads_; i++) {
	  for (int j=0; j < numrows_per_part_; j++) {
	    si_alloc.destroy(test_rowwise_[i] + j);
	  }
	  si_alloc.deallocate(test_rowwise_[i], numrows_per_part_);
	}
	sallocator<set<index_type> *>().deallocate(test_rowwise_, numthreads_);
      }
    }

    friend int load_data(Data& data, string path, int seed, int rank, int numtasks, int numthreads);

    index_type get_num_rows() {
      return num_rows_;
    }

    index_type get_num_cols() {
      return num_cols_;
    }

    index_type num_rows_;
    index_type num_cols_;
    int row_start_index_;

    int *row_perm_;
    int *row_perm_inv_;

    vector<index_type, sallocator<index_type> > train_col_nnzs_;
    index_type **train_row_nnzs_;

    set<index_type> **train_rowwise_;
    set<index_type> **test_rowwise_;

    index_type numthreads_;
    index_type numrows_per_part_;

    vector<vector<index_type>> csc_indices_;
    vector<vector<index_type>> csc_ptrs_;
    vector<index_type> local_nnz_;

    size_t train_total_nnz_;

  };


  int load_data(Data& data, string path, int seed, int rank, int numtasks, int numthreads) {

    // initialize random number generator; 
    // it is important that every machine is using the same rng to partition the data
    rng_type rng(seed);

    cout << "data path: " << path << endl;

    string train_filename, test_filename;
    int num_train, num_test;
    
    //////////////////////////////////////////////////////////
    // Read Metadata
    //////////////////////////////////////////////////////////

    // this scope reads meta file
    {
      char_separator<char> sep(" ");
  
      string metafile_path = path + "/meta";
      ifstream metafile(metafile_path.c_str());

      cout << "reading metadata file: " << metafile_path << endl;
  
      string line;
    
      if (false == metafile.is_open()) {
	cerr << "could not open: " << metafile_path << endl;
	return 1;
      }
    
      // get size of data
      {
	getline(metafile, line);
      
	tokenizer< char_separator<char> > tokens(line, sep);      
	tokenizer< char_separator<char> >::iterator iter = tokens.begin();
      
	data.num_rows_ = lexical_cast<index_type>(*iter);
	cout << "number of rows: " << data.num_rows_ << endl;;
      
	iter++;
	data.num_cols_ = lexical_cast<index_type>(*iter);
	cout << "number of columns: " << data.num_cols_ << endl;
      }
   
      // get information about training data
      {
	getline(metafile, line);
	
	tokenizer< char_separator<char> > tokens(line, sep);      
	tokenizer< char_separator<char> >::iterator iter = tokens.begin();

	num_train = lexical_cast<int>(*iter);

	iter++;
	train_filename = *iter;
      
	cout << "train_filename: " << train_filename
	     << ", number of points: " << num_train << endl;
      }

      // get information about test data
      {
	getline(metafile, line);
	
	tokenizer< char_separator<char> > tokens(line, sep);      
	tokenizer< char_separator<char> >::iterator iter = tokens.begin();
	
	num_test = lexical_cast<int>(*iter);
      
	iter++;
	test_filename = *iter;
	
	cout << "test_filename: " << test_filename
	     << ", number of points: " << num_test << endl;
      }

    } // metafile read done

    data.train_total_nnz_ = num_train;

    //////////////////////////////////////////////////////////
    // Partition Data
    //////////////////////////////////////////////////////////

    // sample a row index permutation
    data.row_perm_ = sallocator<int>().allocate(data.num_rows_);
    data.row_perm_inv_ = sallocator<int>().allocate(data.num_rows_);
    std::iota(data.row_perm_, data.row_perm_ + data.num_rows_, 0);
    // BUGBUG: fixed a bug just now... :( Previous results were not properly shuffled
    std::shuffle(data.row_perm_, data.row_perm_ + data.num_rows_, rng);
    for (int i=0; i < data.num_rows_; i++) {
      data.row_perm_inv_[data.row_perm_[i]] = i;
    }
    // cout << "BUGBUG: data.row_perm_[0] = " << data.row_perm_[0] << endl;
    // cout << "BUGBUG: data.row_perm_inv_[0] = " << data.row_perm_inv_[0] << endl;
    // cout << "BUGBUG: data.row_perm_[1] = " << data.row_perm_[1] << endl;
    // cout << "BUGBUG: data.row_perm_inv_[1] = " << data.row_perm_inv_[1] << endl;


    const int numparts = numtasks * numthreads;
    const int numrows_per_part = data.num_rows_ / numparts 
      + ((data.num_rows_ % numparts > 0) ? 1 : 0);
    data.numrows_per_part_ = numrows_per_part;
    data.numthreads_ = numthreads;

    int row_start_index = numrows_per_part * rank * numthreads;
    int row_end_index = std::min(numrows_per_part * (rank+1) * numthreads, data.num_rows_);
    data.row_start_index_ = row_start_index;
    
    sallocator<vector<int>> svint_alloc;

    // allocate temporary data structure
    vector<int> **colwise_vecs = sallocator< vector<int> * >().allocate(numthreads);
    for (int i=0; i < numthreads; i++) {
      colwise_vecs[i] = svint_alloc.allocate(data.num_cols_);
      for (int j=0; j < data.num_cols_; j++) {
	svint_alloc.construct(colwise_vecs[i] + j);
      }
    }


    //////////////////////////////////////////////////////////
    // Allocate Memories
    //////////////////////////////////////////////////////////
    {
      data.train_col_nnzs_.resize(data.num_cols_, 0);
      data.train_row_nnzs_ = sallocator<index_type *>().allocate(numthreads);
      for (int i=0; i < numthreads; i++) {
	data.train_row_nnzs_[i] = sallocator<index_type>().allocate(numrows_per_part);
	std::fill_n(data.train_row_nnzs_[i], numrows_per_part, 0);
      }
      data.train_rowwise_ = sallocator<set<index_type> *>().allocate(numthreads);
      sallocator<set<index_type>> si_alloc;
      for (int i=0; i < numthreads; i++) {
	data.train_rowwise_[i] = si_alloc.allocate(numrows_per_part);
	for (int j=0; j < numrows_per_part; j++) {
	  si_alloc.construct(data.train_rowwise_[i] + j);
	}
      }

      data.test_rowwise_ = sallocator<set<index_type> *>().allocate(numthreads);
      for (int i=0; i < numthreads; i++) {
	data.test_rowwise_[i] = si_alloc.allocate(numrows_per_part);
	for (int j=0; j < numrows_per_part; j++) {
	  si_alloc.construct(data.test_rowwise_[i] + j);
	}
      }
    }

    //////////////////////////////////////////////////////////
    // Read Training Data
    //////////////////////////////////////////////////////////
    {
      data.local_nnz_.resize(numthreads, 0);

      string train_path = path + "/" + train_filename;

      ifstream file(train_path.c_str());

      if (false == file.is_open()) {
	cerr << "could not open: " << train_path << endl;
	return 1;
      }

      boost::char_separator<char> sep(" ");
      string line;
      int count = 0;

      while (file.good()) {

	if (count % 1000000 == 0) {
	  cout << "reading train: " << count << " / " << num_train << " (" << 
	    (static_cast<double>(count)/num_train * 100) << "%)" << endl;
	}

	getline(file, line);

	tokenizer< char_separator<char> > tokens(line, sep);      
	tokenizer< char_separator<char> >::iterator iter = tokens.begin();

	if (iter == tokens.end()) {
	  break;
	}

	// all indicies are subtracted by 1 to make it 0-based index
	int row_index = lexical_cast<int>(*iter) - 1;
	++iter;

	if (iter == tokens.end()) {
	  break;
	}

	int col_index = lexical_cast<int>(*iter) - 1;
	++iter;

	int perm_row_index = data.row_perm_[row_index];

	if (perm_row_index >= row_start_index
	    && perm_row_index < row_end_index) {
	  int thread_index = (perm_row_index - row_start_index) / numrows_per_part;
	  int local_row_index = (perm_row_index - row_start_index) % numrows_per_part;
	  colwise_vecs[thread_index][col_index].push_back(local_row_index);
	  data.train_rowwise_[thread_index][local_row_index].insert(col_index);

	  data.train_row_nnzs_[thread_index][local_row_index]++;

	  data.local_nnz_[thread_index]++;
	}

	data.train_col_nnzs_[col_index]++;

	count++;

      }

    }

    //////////////////////////////////////////////////////////
    // Make Training Data Compact
    //////////////////////////////////////////////////////////

    cout << "rank: " << rank << ", compact data" << endl;

    data.csc_ptrs_.resize(numthreads, vector<index_type>(data.num_cols_+1, 0));    

    data.csc_indices_.resize(numthreads, vector<index_type>());
    for (int i=0; i < numthreads; i++) {
      data.csc_indices_[i].reserve(data.local_nnz_[i]);
    }

    for (int thread_index=0; thread_index < numthreads; thread_index++) {
      int current_pos = 0;
      for (int col_index=0; col_index < data.num_cols_; col_index++) {
	data.csc_ptrs_[thread_index][col_index] = current_pos;
	for (int row_index : colwise_vecs[thread_index][col_index]) {
	  data.csc_indices_[thread_index].push_back(row_index);
	  current_pos++;
	}
      }
      data.csc_ptrs_[thread_index][data.num_cols_] = current_pos;
    }

    //////////////////////////////////////////////////////////
    // Read Test Data
    //////////////////////////////////////////////////////////
    {
      string test_path = path + "/" + test_filename;

      ifstream file(test_path.c_str());

      if (false == file.is_open()) {
	cerr << "could not open: " << test_path << endl;
	return 1;
      }

      boost::char_separator<char> sep(" ");
      string line;
      int count = 0;

      while (file.good()) {

	if (count % 1000000 == 0) {
	  cout << "reading train: " << count << " / " << num_test << " (" << 
	    (static_cast<double>(count)/num_test * 100) << "%)" << endl;
	}

	getline(file, line);

	tokenizer< char_separator<char> > tokens(line, sep);      
	tokenizer< char_separator<char> >::iterator iter = tokens.begin();

	if (iter == tokens.end()) {
	  break;
	}

	// all indicies are subtracted by 0 to make it 0-based index
	int row_index = lexical_cast<int>(*iter) - 1;
	++iter;

	if (iter == tokens.end()) {
	  break;
	}

	int col_index = lexical_cast<int>(*iter) - 1;
	++iter;

	int perm_row_index = data.row_perm_[row_index];

	if (perm_row_index >= row_start_index
	    && perm_row_index < row_end_index) {
	  int thread_index = (perm_row_index - row_start_index) / numrows_per_part;
	  int local_row_index = (perm_row_index - row_start_index) % numrows_per_part;
	  data.test_rowwise_[thread_index][local_row_index].insert(col_index);
	}

	count++;

      }

    }


    // deallocate temporary data structure
    for (int i=0; i < numthreads; i++) {
      for (int j=0; j < data.num_cols_; j++) {
	svint_alloc.destroy(colwise_vecs[i] + j);
      }
      svint_alloc.deallocate(colwise_vecs[i], data.num_cols_);
    }
    sallocator< vector<int> * >().deallocate(colwise_vecs, numthreads);

    return 0;
  
  }


}



#endif
